from contextlib import contextmanager
import selenium
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException


DEFAULT_WAIT_TIME_SEC = 20

NOT_HEADLESS_USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.50 Safari/537.36'

class ChromeBrowser(webdriver.Chrome):
    def __init__(self, show_browser=False, wait_time_sec=DEFAULT_WAIT_TIME_SEC):
        o = selenium.webdriver.chrome.options.Options()
        o.headless = not show_browser
        o.add_argument('log-level=3')
        o.add_argument('--no-sandbox')
        o.add_argument('--disable-dev-shm-usage')
        o.add_argument("window-size=1920,1080")

        if not show_browser:
            o.add_argument('user-agent={}'.format(NOT_HEADLESS_USER_AGENT))

        super().__init__(chrome_options=o)

        self.implicitly_wait(wait_time_sec)

    def click_element(self, xpath_to_element):
        e = self.find_element_by_xpath(xpath_to_element)
        e.click()

    def is_xpath_exist(self, xpath_to_element):
        try:
            self.find_element_by_xpath(xpath_to_element)
        except NoSuchElementException:
            return False
        return True

    def switch_to_window_by_index(self, index):
        self.switch_to.window(self.window_handles[index])

    def screenshot_all_windows(self):
        FILE_NAME_FORMAT = 'screenshot_window_{}.png'

        filenames = []

        for i, window_handle in enumerate(self.window_handles):
            self.switch_to.window(window_handle)

            filename = FILE_NAME_FORMAT.format(i)
            self.save_screenshot(filename)
            
            filenames.append(filename)

        return filenames


@contextmanager
def open_chrome_browser(*args, **kwargs):
    browser = ChromeBrowser(*args, **kwargs)
    try:
        yield browser
    finally:
        browser.close()
