import platform
import subprocess
import winreg
import requests
from urllib.request import urlopen
from io import BytesIO
import zipfile
from logger_utils import init_logger


LOGGER = init_logger(__name__)

CHROMEDRIVER_FILENAME = 'chromedriver.exe'


###############################################################################
# Zip Utils
###############################################################################
def _extract_file_from_zipfile(zipfile_obj, filename_to_extract):
    res = []
    for line in zipfile_obj.open(filename_to_extract).readlines():
        res.append(line)

    return b''.join(res)

def extract_file_from_zip(zip_data, filename_to_extract, output_filepath=None):
    """
    Extract a file from the zip file buffer.
    The file will be extracted to output_filepath (if given).
    Returns the extracted file's content.
    """

    zipf = zipfile.ZipFile(BytesIO(zip_data))
    extract_data = _extract_file_from_zipfile(zipf, filename_to_extract)
    
    if output_filepath:
        with open(output_filepath, 'wb') as f:
            f.write(extract_data)

    return extract_data


###############################################################################
# Get Version
###############################################################################
def get_local_chrome_version():
    """Returns the version of the local chrome browser, or None if it doesn't exist"""

    CHROME_VERSION_KEY = r'Software\Google\Chrome\BLBeacon'
    registry = winreg.ConnectRegistry(None, winreg.HKEY_CURRENT_USER)

    hkey = winreg.OpenKey(registry, CHROME_VERSION_KEY)
    try:
        value = winreg.EnumValue(hkey, 0)
        assert value[0] == 'version'
        
        local_chrome_version = value[1]
        LOGGER.debug('Local chrome version: {}'.format(local_chrome_version))
        return local_chrome_version
    finally:
        winreg.CloseKey(hkey)

    LOGGER.error('Chrome version was not found')

def get_local_chromedriver_version(chromedriver_filename=CHROMEDRIVER_FILENAME):
    """Returns the version of the local chromedriver, or None if it doesn't exist"""

    try:
        p = subprocess.run([chromedriver_filename, '--version'], 
            universal_newlines=True, stdout=subprocess.PIPE)
        chromedriver_version = p.stdout.split()[1]
        LOGGER.debug('Local chromedriver version: {}'.format(chromedriver_version))
        return chromedriver_version
    except FileNotFoundError:
        LOGGER.debug('Chromedriver does not exist')
        return None

def get_latest_available_chromedriver_version(chrome_version=None):
    """
    Returns the latest available chromedriver version (online) that supports the given chrome version.
    If the chrome version is not given, the local chrome version will be used.
    """

    # Use the local chrome version if no version was given
    if not chrome_version:
        chrome_version = get_local_chrome_version()
            
    chrome_version = '.'.join(chrome_version.split('.')[:-1])
    
    LATEST_RELEASE_URL = 'https://chromedriver.storage.googleapis.com/LATEST_RELEASE_{}'.format(chrome_version)

    res = requests.get(LATEST_RELEASE_URL)
    assert res.status_code == 200, 'request failed'
    
    latest_available_chromedriver_version = res.text
    LOGGER.debug('Latest available chromedriver version online is {} (that supports chrome {})'.format(
        latest_available_chromedriver_version, chrome_version
    ))
    return latest_available_chromedriver_version


###############################################################################
# Download chromedriver
###############################################################################
def _get_chromedriver_zip_url(chromedriver_version):
    os_platform = {
        'Windows':  'win',
        'Linux':    'linux',
        'Darwin':   'mac',
    }[platform.system()]

    os_bitness = platform.architecture()[0].replace('bit', '')

    os_type = '{}{}'.format(os_platform, os_bitness if os_platform == 'linux' else '32') # chromedriver 64bit only exists for linux

    return 'https://chromedriver.storage.googleapis.com/{}/chromedriver_{}.zip'.format(
        chromedriver_version, os_type)

def download_chromedriver(chromedriver_version=None, output_filepath=CHROMEDRIVER_FILENAME, force_download=False):
    """
    Download chromedriver of version chromedriver_version and save it in output_filepath.
    If chromedriver_version is None, the latest one available (for the local chrome browser version) will be used.
    If force_download=True, chromedriver will always be downloaded, otherwise it won't download if that version already exists locally.
    """
    if not chromedriver_version:
        chromedriver_version = get_latest_available_chromedriver_version()

    LOGGER.debug('Downloading chromedriver {}'.format(chromedriver_version))

    if not force_download:
        LOGGER.debug('Checking the local chromedriver version')
        if get_local_chromedriver_version() == chromedriver_version:
            LOGGER.debug('Chromedriver {} already exits'.format(chromedriver_version))
            return
    
    download_url = _get_chromedriver_zip_url(chromedriver_version)
    
    LOGGER.debug('Downloading ZIP from {}'.format(download_url))
    res = urlopen(download_url)

    LOGGER.debug('Extracting {} to {}'.format(CHROMEDRIVER_FILENAME, output_filepath))
    extract_file_from_zip(res.read(), CHROMEDRIVER_FILENAME, output_filepath)


###############################################################################
# Main
###############################################################################
if __name__ == "__main__":
    download_chromedriver()
