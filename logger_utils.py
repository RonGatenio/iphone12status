import os
import logging


def init_logger(logger_name, logger_file_path=None, use_telegram=True):
    # set logger_file_path if needed
    if not logger_file_path:
        logger_file_path = os.path.join('logs', '{}.log'.format(logger_name))

    # make logs folder if needed
    if not os.path.exists(os.path.dirname(logger_file_path)):
        os.mkdir(os.path.dirname(logger_file_path))

    # create logger
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.DEBUG)

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # add console handler
    sh = logging.StreamHandler()
    sh.setLevel(logging.DEBUG)
    sh.setFormatter(formatter)
    logger.addHandler(sh)

    # add file handler
    fh = logging.FileHandler(logger_file_path)
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    return logger
