
# Read more about telegram API in: https://core.telegram.org/bots/api

from io import BytesIO
from math import ceil
import requests


REQUEST_SEND_MESSAGE  = 'sendMessage'
REQUEST_SEND_PHOTO    = 'sendPhoto'
REQUEST_SEND_GROUP_PHOTOS = 'sendMediaGroup'
REQUEST_SEND_LOCATION = 'sendLocation'

BOT_API_URL = 'https://api.telegram.org/bot{bot_api_key}/{request}'


class MediaGroup:
    def __init__(self):
        self._group = []

    def get_media_blocks(self, size=10):
        round_up = int(ceil(len(self._group) * 1.0 / size)) * size
        for i in range(0, round_up, size):
            yield self._group[i:i+size]

    def _add(self, type, media, caption, parameters):
        parameters['type'] = type
        parameters['media'] = media
        parameters['caption'] = caption
        self._group.append(parameters)

    def add_photo(self, media, caption=None, **parameters):
        self._add('photo', media, caption, parameters)


class TelegramNotifier:
    def __init__(self, bot_api_key=None, chat_id=None):
        self._bot_api_key = bot_api_key
        self._chat_id = chat_id

    def is_enabled(self):
        return self._bot_api_key and self._chat_id

    def post(self, request, _to_json=False, **parameters):
        if not self.is_enabled():
            return

        url = BOT_API_URL.format(bot_api_key=self._bot_api_key, request=request)

        data = {}
        files = {}
        for k, v in parameters.items():
            if hasattr(v, 'read'):
                files[k] = v
            else:
                data[k] = v

        data['chat_id'] = self._chat_id

        if _to_json:
            response = requests.post(url, json=data, files=files)
        else:
            response = requests.post(url, data=data, files=files)

        if response.status_code != 200:
            print('Fail request {} {} - {} ({})'.format(
                request,
                response.status_code, 
                response.reason,
                response.url))

        return response

    def send_message(self, text, **parameters):
        return self.post(REQUEST_SEND_MESSAGE, text=text, **parameters)

    def send_photo_by_url(self, url, caption=None, **parameters):
        return self.post(REQUEST_SEND_PHOTO, photo=url, caption=caption, **parameters)

    def send_media_group(self, media_group, group_size=10):
        for g in media_group.get_media_blocks(group_size):
            self.post(REQUEST_SEND_GROUP_PHOTOS, _to_json=True, media=g)

    def send_photo_by_filename(self, filename, caption=None, **parameters):
        with open(filename, 'rb') as photo:
            return self.post(REQUEST_SEND_PHOTO, photo=photo, caption=caption, **parameters)

    def send_photo_by_bytes(self, photo_bytes, caption=None, **parameters):
        with BytesIO(photo_bytes) as photo:
            return self.post(REQUEST_SEND_PHOTO, photo=photo, caption=caption, **parameters)

    def send_location(self, longitude, latitude, **parameters):
        return self.post(REQUEST_SEND_LOCATION, longitude=longitude, latitude=latitude, **parameters)
