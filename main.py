import argparse
import reporter
from telegram_notifier import TelegramNotifier


def parse_arguments(args=None):
    parser = argparse.ArgumentParser()

    parser.add_argument('-t', '--telegram',
                        nargs=2,
                        metavar=('BOT_API_KEY', 'CHANNEL_NAME'),
                        default=(None, None),
                        help='Telegram bot parameters')

    parser.add_argument('-d', '--download_chromedriver',
                        action='store_true',
                        default=False,
                        help='Download the chromedriver if needed')

    parser.add_argument('-v', '--show_browser',
                        action='store_true',
                        default=False,
                        help='Show browser while running')

    return parser.parse_args(args)


def main_cli(args=None):
    args = parse_arguments(args)

    if args.download_chromedriver:
        from chrome_driver.download_chromedriver import download_chromedriver
        download_chromedriver()

    telegram_notifier = TelegramNotifier(*args.telegram)

    reporter.report(telegram_notifier, args.show_browser)


if __name__ == "__main__":
    main_cli()
