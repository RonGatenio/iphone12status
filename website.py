from time import sleep
from abc import ABC, abstractmethod
from telegram_notifier import MediaGroup


SLEEP_TIME_LONG_SEC = 4


class Site(ABC):
    URL = None
    NAME = None

    def __init__(self, browser, telegram_notifier):
        self._browser = browser
        self._telegram = telegram_notifier
    
    @classmethod
    def caption_markdown(cls):
        return f'[{cls.NAME}]({cls.URL})'

    def capture_screenshot(self):
        data = self._browser.get_screenshot_as_png()
        self._telegram and self._telegram.send_photo_by_bytes(data, caption=self.caption_markdown(), parse_mode='Markdown')

    def send_image(self, img):
        img_url = img.get_attribute('src')
        if img_url:
            self._telegram and self._telegram.send_photo_by_url(img_url, caption=self.caption_markdown(), parse_mode='Markdown')

    def send_all_images(self, img_elements):
        m = MediaGroup()
        for i, img in enumerate(img_elements):
            img_url = img.get_attribute('src')
            if img_url:
                m.add_photo(img_url, caption=f'{self.caption_markdown()} ({i + 1})', parse_mode='Markdown')
        self._telegram and self._telegram.send_message(self.caption_markdown(), parse_mode='Markdown')
        self._telegram and self._telegram.send_media_group(m)

    def go_to_url(self):
        self._browser.get(self.URL)

    @abstractmethod
    def collect(self):
        pass


class SimpleCaptureSite(Site):
    def collect(self):
        self.capture_screenshot()


class SimpleCaptureSiteSleep(Site):
    def collect(self):
        sleep(getattr(self, 'SLEEP', SLEEP_TIME_LONG_SEC))
        self.capture_screenshot()
