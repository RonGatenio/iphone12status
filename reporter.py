from time import sleep
from website import Site, SimpleCaptureSite, SimpleCaptureSiteSleep
from chrome_driver.chrome_browser import open_chrome_browser


SLEEP_TIME_DEFAULT_SEC = 1


class SitePelephone(SimpleCaptureSite):
    URL = 'https://www.pelephone.co.il/digitalsite/heb/shop/campaigns/apple_store/'
    NAME = 'Pelephone'


class SiteCellcom(Site):
    URL = 'https://cellcomshop.cellcom.co.il/'
    NAME = 'Cellcom'

    def collect(self):
        slider = self._browser.find_elements_by_xpath('//div[contains(@class, "magestore-bannerslider-standard")]')[-1]
        imgs = slider.find_elements_by_xpath('.//ul[@class="slides"]/li/a/img')
        self.send_all_images(imgs)


class SiteHotMobile(SimpleCaptureSite):
    URL = 'https://www.hotmobile.co.il/pages/default.aspx#'
    NAME = 'HOT Mobile'


class SitePartner(SimpleCaptureSiteSleep):
    URL = 'https://store.partner.co.il/home'
    NAME = 'Partner'


class SiteBezeq(SimpleCaptureSite):
    URL = 'https://bstore.bezeq.co.il/mobiles/apple/?WT.isp=menu'
    NAME = 'Bezeq'


class SiteElectronics(SimpleCaptureSiteSleep):
    URL = 'https://www.payngo.co.il/'
    NAME = 'Electronics Store'

    def collect(self):
        img = self._browser.find_element_by_xpath('//div[@class="homepage"]/a/img')
        self.send_image(img)


class SiteElectronicsStore(SimpleCaptureSiteSleep):
    URL = 'https://www.payngo.co.il/computers-pcs/smartphones/smartphones.html?mvtg=iphone'
    NAME = 'Electronics Store'


class SiteIDigital(SimpleCaptureSite):
    URL = 'https://www.idigital.co.il/store/idigital_apple/store_iphone/iphone12/'
    NAME = 'iDigital'


class SiteIStore(SimpleCaptureSite):
    URL = 'https://www.istoreil.co.il/'
    NAME = 'iStore'


class SiteKSP(SimpleCaptureSite):
    URL = 'https://ksp.co.il/'
    NAME = 'KSP'


class SiteIvory(SimpleCaptureSiteSleep):
    URL = 'https://www.ivory.co.il/'
    NAME = 'Ivory'
    SLEEP = 8


class SiteApple(SimpleCaptureSite):
    URL = 'https://www.apple.com/il/'
    NAME = 'Apple IL'


SITES = [
    SiteApple,
    SitePelephone,
    SiteCellcom,
    SiteHotMobile,
    SitePartner,
    SiteBezeq,
    SiteElectronics,
    SiteElectronicsStore,
    SiteIDigital,
    SiteIStore,
    SiteKSP,
    SiteIvory,
]


def report(telegram_notifier, show_browser=False):
    with open_chrome_browser(show_browser=show_browser) as browser:
        for site_cls in SITES:
            try:
                site = site_cls(browser, telegram_notifier)
                site.go_to_url()
                sleep(SLEEP_TIME_DEFAULT_SEC)
                site.collect()
            except:
                telegram_notifier.send_message(f'Failed to report on {site_cls.caption_markdown()}', parse_mode='Markdown')
